import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenTrusPage } from './open-trus.page';

describe('OpenTrusPage', () => {
  let component: OpenTrusPage;
  let fixture: ComponentFixture<OpenTrusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenTrusPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenTrusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
