# OpenTrus

A  website that provides information on the standards of products, services and companies.

  ![Alt text](opentrus.png)
  
## How to install

### Prerequisites:

Install Nodejs & npm on computer from https://nodejs.org/.

Using npm, install Ionic Framework with command below or as detailed at https://beta.ionicframework.com/docs/installation/cli.

``npm install -g ionic``

### Running the app


Clone repo:

``git clone https://gitlab.com/opentrus-org/opentrus.git``

Change into 'opentrus' and then the 'myApp' directory.

``cd myApp``

Run ionic serve to open the app in default browser.

``ionic serve``


## What OPENTRUS stands for:

OPENTRUS Product Development Guidelines

### Open / Transparent / Truthful

Either:

    Open source

    Transparent and open to third party auditing

The company or team developing the product should be very honest and open to questions from the public and from people. It is usually OK to make mistakes as long as you are honest about them, as long as trust is there.

### Privacy Respecting

Either:

    No tracking

    Completely transparent telemetry –

        where the end user can opt-out completely AND

        where the end user can see exactly what the telemetry being ‘sent home’ is; AS well as what kind of inferred information could be known by the developers company through the use of detective work, or cross-comparison with other third party information about the end-user.

        If there is any tracking, it should be   completely consensual, and the data collected should be opened up    for everyone to see for scientific purposes. (anonymised properly    of course.)

### Ethical and excellent to people.

Making sure that the life of all stakeholders is improved if possible, taking consideration of the most veritable knowledge on ergonomics, psychology, and hopes of all living beings.

    Workers - fair compensation, low CEO:worker salary ratio, democratic control of the company.

    consumers

    the public (both now and in the future generations)

    biological life.

It should also be a product/service that is uplifting to humankind and the planet to be deemed excellent.

This mission of the product should be to reduce suffering of people and all life.

### Non-harmful

Not allowing the product to be used for harmful or violent purposes, preventing end-users from using the product for harmful purposes.

### Taking responsibility for environmental impact

Developers should make attempts to assess the environmental impact of the product, take responsibility for it, and reduce it to as low as can physically be.

Animal life, planet system functioning, plastic waste, etc.

If you discover, even years after release of the product, that you’re product unnecessarily produces too much energy wastage or is in any other way harmful, then you should let people know publicly.

### Repairable and rugged.

As much as is possible, developers should design products that can be repaired and upgraded by end-users in the future, allowing a reduction in built-in obsolescence.

Very in depth documentation should be created and provided for the product, in order to allow for repair.

Parts should be easy to remove.

Parts should be made available to replace broken parts.

Guidance should be given to consumers that provide professional advice on how to lengthen the useful-life of the product.

This is in order to be good to consumers, and will also reduce the amount of landfill.

## Usable (well-designed) and Useful

Usable: Designers and engineers should strive for excellence in the usability and functionality of the product or service. It should do a job well.

As much as is possible, products should be designed to be usable by people of all physical, cognitive, medical and technical ability.

An attempt at making an excellent product, technically and ergonomically, that helps people should be made.

Combining usability with any more advanced capability of the product is recommended to be achieved through a settings option. i.e. in video games, players can choose between Easy, Medium, Hard modes, based on their preference. This could be implemented within the software to combine usability with software capability.

Useful: The product or service being made should be aimed at solving a real problem, uplifting people and strive to be a catalyst for the reduction of suffering on Earth.

A problem’s priority for solving can be determined by measuring the severity of suffering that the problem causes to a living being and comparing the result with other problems that could be worked on.

An additional model to think about the choice of what work to do might be to find a problem that you have some interest, skill or talent in figuring out how to solve and dedicating work toward that.

Usually ‘how much money could be made’ from solving a problem isn’t always a good method of choice. A company might be able to make a lot of money selling super-yachts to multi-millionaires who can’t find luxurious enough boats, but this is not a very worthy problem to spend your time on solving.
### Safe & Secure

The product should be safe to use.

Prioritising continuous hardening of security to avoid user’s private information being viewed.